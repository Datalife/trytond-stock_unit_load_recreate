=================================
Stock Unit Load Recreate Scenario
=================================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard, Report
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> tomorrow = today + relativedelta(days=1)
    >>> time_ = datetime.datetime.now().time()
    >>> time_ = time_.replace(microsecond=0)


Install stock_unit_load_recreate::

    >>> config = activate_modules('stock_unit_load_recreate')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create stock user::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> admin_user = config.user
    >>> stock_user = User()
    >>> stock_user.name = 'Stock'
    >>> stock_user.login = 'stock'
    >>> stock_user.company = company
    >>> stock_group, = Group.find([('name', '=', 'Stock')])
    >>> stock_user.groups.append(stock_group)
    >>> stock_user.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('8')
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])

Create an unit load::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.company != None
    True
    >>> unit_load.start_date != None
    True
    >>> unit_load.end_date != None
    True
    >>> unit_load.end_date = unit_load.start_date + relativedelta(minutes=5)
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = output_loc
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 5
    >>> len(unit_load.production_moves)
    0
    >>> unit_load.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Production location" in "Unit load" is not valid according to its domain. - 
    >>> unit_load.production_location = production_loc
    >>> unit_load.save()
    >>> unit_load.code != None
    True

Add moves::

    >>> unit_load.production_state
    'running'
    >>> unit_load.quantity = Decimal('35.0')
    >>> len(unit_load.production_moves)
    1
    >>> move = unit_load.production_moves[0]
    >>> move.planned_date == today
    True
    >>> move.product == unit_load.product
    True
    >>> move.quantity
    35.0
    >>> move.from_location == production_loc
    True
    >>> not move.to_location
    True
    >>> move.to_location = storage_loc
    >>> move.currency == unit_load.company.currency
    True
    >>> unit_load.save()
    >>> unit_load.state
    'draft'
    >>> unit_load.production_state
    'running'
    >>> len(unit_load.moves)
    1
    >>> len(unit_load.production_moves)
    1
    >>> unit_load.production_moves[0].state
    'draft'
    >>> unit_load.internal_quantity
    35.0
    >>> unit_load.quantity_per_case
    7.0
    >>> unit_load.click('assign')
    >>> unit_load.click('do')

Force production::

    >>> config.user = stock_user.id
    >>> unit_load.click('force_production')
    Traceback (most recent call last):
        ...
    trytond.model.modelview.AccessButtonError: Calling button "force_production on "stock.unit_load" is not allowed. - 
    >>> unit_load.reload()
    >>> unit_load.production_state
    'done'
    >>> config.user = admin_user
    >>> unit_load.click('force_production')
    >>> unit_load.reload()
    >>> unit_load.production_state
    'running'
    >>> len(unit_load.moves)
    1
    >>> unit_load.moves[0].state
    'draft'


Cancel dropped and production moves done UL::

    >>> unit_load.state, unit_load.production_state
    ('draft', 'running')
    >>> unit_load.warehouse = warehouse_loc
    >>> unit_load.save()
    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    >>> len(unit_load.ul_moves)
    1
    >>> drop_ul = Wizard('stock.unit_load.do_drop', [unit_load])
    >>> drop_ul.form.location = production_loc
    >>> drop_ul.form.start_date = drop_ul.form.end_date + relativedelta(minutes=30)
    >>> drop_ul.execute('try_')
    >>> unit_load.reload()
    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    >>> last_move = unit_load.last_moves[0]
    >>> len(unit_load.ul_moves)
    2
    >>> config.user = stock_user.id
    >>> unit_load.click('cancel') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ('UserError', ('You cannot cancel Unit load moves in Done state.', ''))
    >>> config.user = admin_user
    >>> unit_load.click('cancel')
    >>> len(unit_load.ul_moves)
    1
    >>> last_move not in unit_load.last_moves
    True
    >>> bool(unit_load.available)
    True
    >>> unit_load.state, unit_load.production_state
    ('done', 'done')
    >>> unit_load.production_moves == unit_load.moves
    True
    >>> unit_load.click('cancel')
    >>> len(unit_load.ul_moves)
    0
    >>> len(unit_load.moves)
    0

