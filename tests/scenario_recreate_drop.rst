======================================
Stock Unit Load Recreate Drop Scenario
======================================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard, Report
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> tomorrow = today + relativedelta(days=1)
    >>> time_ = datetime.datetime.now().time()
    >>> time_ = time_.replace(microsecond=0)


Install stock_unit_load_recreate::

    >>> config = activate_modules('stock_unit_load_recreate')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create main product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('8')
    >>> product.save()

Create another product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> aux_product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Aux. product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('0')
    >>> template.save()
    >>> aux_product, = template.products
    >>> aux_product.cost_price = Decimal('2')
    >>> aux_product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> new_stor = Location(name='Storage 2')
    >>> new_stor.parent = storage_loc
    >>> new_stor.save()
    >>> storage3 = Location(name='Storage 3')
    >>> storage3.parent = storage_loc
    >>> storage3.save()

Create an unit load::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load.end_date = unit_load.start_date + relativedelta(minutes=15)
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 5
    >>> unit_load.save()

Add moves::

    >>> unit_load.quantity = Decimal('35.0')
    >>> move = unit_load.production_moves[0]
    >>> move.to_location = new_stor
    >>> input_move = unit_load.production_moves.new()
    >>> input_move.product = aux_product
    >>> input_move.quantity = Decimal('4')
    >>> input_move.from_location = storage_loc
    >>> input_move.to_location = production_loc
    >>> output_move = unit_load.production_moves.new()
    >>> output_move.product = aux_product
    >>> output_move.quantity = Decimal('4')
    >>> output_move.from_location = production_loc
    >>> output_move.to_location = new_stor
    >>> output_move.unit_price = aux_product.cost_price
    >>> unit_load.save()
    >>> len(unit_load.production_moves)
    3
    >>> unit_load.click('assign')
    >>> unit_load.click('do')

Drop::

    >>> drop_ul = Wizard('stock.unit_load.do_drop', [unit_load])
    >>> drop_ul.form.start_date = drop_ul.form.end_date - relativedelta(minutes=30)
    >>> drop_ul.form.location = production_loc
    >>> drop_ul.execute('try_')
    >>> len(drop_ul.form.products)
    1
    >>> not drop_ul.form.products[0].location
    True
    >>> drop_ul.form.location = storage3
    >>> drop_ul.form.products[0].location != None
    True
    >>> drop_ul.execute('force')
    >>> unit_load.reload()
    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    >>> bool(unit_load.dropped)
    True

Execute drop wizard again::

    >>> drop_ul = Wizard('stock.unit_load.do_drop', [unit_load])
    >>> drop_ul.execute('undo_')
    >>> drop_ul.form.drop_cases_quantity = 2.0
    >>> drop_ul.execute('do_')
    >>> unit_load.reload()
    >>> bool(unit_load.dropped)
    False
    >>> unit_load.available_cases_quantity
    3.0
    >>> unit_load.last_moves == unit_load.drop_moves
    True
    >>> drop_ul = Wizard('stock.unit_load.do_drop', [])
    >>> drop_ul.form.ul_code = unit_load.code
    >>> drop_ul.execute('pre_data')
    >>> bool(drop_ul.form.available)
    True
    >>> drop_ul.execute('data')
    >>> drop_ul.form.location = production_loc
    >>> drop_ul.form.drop_cases_quantity
    3.0
    >>> drop_ul.execute('try_')
    >>> drop_ul.form.location = storage3
    >>> drop_ul.execute('force')
    >>> unit_load.reload()
    >>> unit_load.available_cases_quantity
    0.0
    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    >>> bool(unit_load.dropped)
    True

Create another unit load::

    >>> unit_load = UnitLoad()
    >>> unit_load.start_date = datetime.datetime.now() - relativedelta(days=1)
    >>> unit_load.end_date = unit_load.start_date + relativedelta(minutes=15)
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 1
    >>> unit_load.save()

Execute drop wizard again::

    >>> bool(unit_load.drop_moves and unit_load.last_moves)
    False
    >>> drop_ul = Wizard('stock.unit_load.do_drop', [])
    >>> drop_ul.form.ul_code = unit_load.code
    >>> drop_ul.execute('pre_data') # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserError: Unit load "..." is not available. - 