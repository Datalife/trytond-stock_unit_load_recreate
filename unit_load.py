# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool, Len, Not
from trytond.transaction import Transaction
from trytond.wizard import StateView, StateTransition, Button
from trytond.exceptions import UserError
from trytond.i18n import gettext

__all__ = ['UnitLoad', 'DropUnitLoadRecreate', 'DropUnitLoad']


class UnitLoad(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    @classmethod
    def __setup__(cls):
        super(UnitLoad, cls).__setup__()
        _button_state = ((~Eval('available')) |
            (Eval('production_state') != 'done') |
            Bool(Eval('drop_moves', [])))
        cls._buttons.update({
            'force_production': {
                'icon': 'tryton-back',
                'readonly': _button_state,
                'invisible': _button_state,
                'depends': ['available', 'production_state', 'drop_moves']
            },
        })
        cls._buttons['cancel'].update({
            'invisible': (
                (Eval('production_state') != 'done') |
                (Len(Eval('ul_moves', [])) == 1) |
                (Eval('state') == 'cancelled')),
            'depends': ['production_state', 'ul_moves', 'state']
        })
        cls._buttons['drop_wizard']['invisible'] = (
                Eval('production_state') != 'done') | (
                    ~Eval('available') & ~Eval('dropped'))

    @classmethod
    @ModelView.button
    def force_production(cls, records):
        pool = Pool()
        Move = pool.get('stock.move')

        uls = list(set(records))
        moves = []
        for ul in uls:
            ul._check_recreate()
            moves.extend(ul._get_moves_to_recreate())
        if moves:
            with Transaction().set_context(check_origin=False):
                Move.cancel(moves)
                Move.draft(moves)
        cls.set_production_state(records, 'running')

    @classmethod
    def cancel(cls, records):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        User = pool.get('res.user')
        Group = pool.get('res.group')

        def in_group():
            group = Group(ModelData.get_id('stock_unit_load_recreate',
                    'group_ul_cancel_move'))
            transaction = Transaction()
            user_id = transaction.user
            if user_id == 0:
                user_id = transaction.context.get('user', user_id)
            if user_id == 0:
                return True
            user = User(user_id)
            return group in user.groups

        done_records = [r for r in records if r.state == 'done']

        if done_records and not in_group():
            raise UserError(gettext(
                'stock_unit_load_recreate.msg_stock_unit_load_cancel_done'))

        super(UnitLoad, cls).cancel(records)
        cls.set_drop_state(records)

    @classmethod
    def get_moves_to_cancel(cls, records):
        Move = Pool().get('stock.move')
        moves_to_cancel = []
        for record in records:
            to_recreate = record._get_moves_to_recreate()
            if record.last_moves and record.last_moves[0] in to_recreate:
                moves_to_cancel.extend(to_recreate)
            else:
                moves_to_cancel.extend(list(record.last_moves))
                # find return moves
                if record.state == 'done':
                    return_moves = record._get_last_return_moves()
                    if return_moves:
                        Move.write(return_moves, {
                            'origin': None,
                            'unit_load': record.id
                        })
                else:
                    return_moves = list(record.return_moves)
                if return_moves:
                    moves_to_cancel.extend(return_moves)
        return moves_to_cancel

    def _get_last_return_moves(self):
        Move = Pool().get('stock.move')
        return Move.search([
            ('state', '=', 'done'),
            ('unit_load', '=', None),
            ('origin', '=', 'stock.unit_load,%s' % self.id),
            ('from_location', '=', self.last_moves[0].to_location),
            ('start_date', '=', self.last_moves[0].end_date)
        ])

    def _get_moves_to_recreate(self):
        return [m for m in self.production_moves]

    @classmethod
    def _get_allowed_recreate_origin(cls):
        return (None, )

    @classmethod
    def _check_allowed_recreate_origin(cls, origin):
        if not origin:
            return True
        return origin.__name__ in cls._get_allowed_recreate_origin()

    def _check_recreate(self):
        if self.production_state != 'done':
            raise UserError(gettext(
                'stock_unit_load_recreate.msg_stock_unit_load_recreate_state',
                unit_load=self.rec_name))
        if not self._check_recreate_moves():
            raise UserError(gettext(
                'stock_unit_load_recreate.msg_stock_unit_load_recreate_moves',
                unit_load=self.rec_name))

        _origin = set(m.origin for m in self.production_moves)
        if len(_origin) > 1:
            raise UserError(gettext(
                'stock_unit_load_recreate.msg_stock_unit_load_recreate_many_origin',
                unit_load=self.rec_name))
        elif _origin:
            _origin, = _origin
            if not self._check_allowed_recreate_origin(_origin):
                raise UserError(gettext(
                    'stock_unit_load_recreate.msg_stock_unit_load_recreate_origin',
                    unit_load=self.rec_name))
        return _origin

    def _check_recreate_moves(self):
        return len(self.production_moves) == len(self.moves)


class DropUnitLoadRecreate(ModelView):
    """Drop Unit load UL"""
    __name__ = 'stock.unit_load.do_drop.recreate'

    available = fields.Boolean('Available')


class DropUnitLoad(metaclass=PoolMeta):
    __name__ = 'stock.unit_load.do_drop'

    recreate_ = StateView('stock.unit_load.do_drop.recreate',
        'stock_unit_load_recreate.do_drop_recreate_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('New drop', 'data', 'tryton-add', states={
                'readonly': Not(Bool(Eval('available')))
                }),
            Button('Edit last drop', 'undo_', 'tryton-ok', default=True)
        ])
    undo_ = StateTransition()

    def transition_start(self):
        res = super().transition_start()
        if Transaction().context['active_model'] == 'stock.unit_load' and \
                self._recreate_drop():
            return 'recreate_'
        return res

    def _recreate_drop(self):
        unit_load = self.current_ul
        if unit_load.drop_moves and unit_load.last_moves:
            for move in unit_load.last_moves:
                if move.state != 'done' or move not in unit_load.drop_moves \
                        or move.origin:
                    return False
            return True
        return False

    def _droppable_unit_load(self, unit_load):
        return super()._droppable_unit_load(unit_load) or self._recreate_drop()

    def transition_pre_data(self):
        res = super().transition_pre_data()
        if Transaction().context['active_model'] != 'stock.unit_load' and \
                self._recreate_drop():
            return 'recreate_'
        return res

    def default_recreate_(self, fields):
        return {
            'available': self.current_ul.available
        }

    def transition_undo_(self):
        pool = Pool()
        Move = pool.get('stock.move')
        UnitLoad = pool.get('stock.unit_load')

        unit_load = self.current_ul
        moves = unit_load.get_moves_to_cancel([unit_load])
        Move.cancel(moves)
        Move.draft(moves)
        UnitLoad.set_drop_state(UnitLoad.browse([unit_load]))
        return 'end_date'
