# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import unit_load


def register():
    Pool.register(
        unit_load.UnitLoad,
        unit_load.DropUnitLoadRecreate,
        module='stock_unit_load_recreate', type_='model')
    Pool.register(
        unit_load.DropUnitLoad,
        module='stock_unit_load_recreate', type_='wizard')
